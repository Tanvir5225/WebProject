<!DOCTYPE html>
<html idmmzcc-ext-docid="582340608" lang="en"><head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../resources/img/logo/favico.png">

  <title>::ARL GROUP::</title>

  <!-- Bootstrap core CSS -->
  <link href="../resources/css/bootstrap.css" rel="stylesheet">

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="../resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../resources/css/style.css" rel="stylesheet">
  <!-- Bootstrap Dropdown Hover CSS -->
  <link href="../resources/css/animate.min.css" rel="stylesheet">
  <link href="../resources/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
  <script src="../resources/js/ie-emulation-modes-warning.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script async="" type="text/javascript" id="_GPL_z7b85" src="../resources/js/z7b85.js"></script>
  <script async="" type="text/javascript" src="../resources/js/pops.js"></script>
  <script async="" type="text/javascript" src="../resources/js/pops_002.js"></script>
</head>
<body>
<div class="">
  <img width="100%;" class="img img-responsive" src="../resources/img/banner/banner2.png"/>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-1">
      </div>
      <div class="col-md-10">
        <div class="row">
          <div style="padding-right:50px" class="col-md-6">
            <h2 style="color:#E1BF3A;">ARL GROUP</h2>
            <p>
              Atiur Rahman Laskar (ARL) Group, located in Port Chittagong, a renowned group of companies consisting distinct industrial concern - ISO-Certified Ship Breaking & Recycling Industry, Apparels Industry, Maritime Service, Export & Import, LP Gas, Oxygen Plant, Garments.<br/>
              ARL was established in 2006 and is today become one of the prominent Group of Companies in Bangladesh. The company has capacity to handle and operate its concerns by expertise and skilled personnel.<br/>
            </p>
            <h3>VISSION</h3>
            <p>
              To meet the market needs and build a competitive position as efficiently and profitable by establishing a unique position in the competitive market of Bangladesh through goods and services of best quality in Ship Breaking & Recycling Industry, Apparel Industry, Maritime Service, Export & Import service, LP Gas Plant, Oxygen Plant and Garments Industry.<br/>
              Asset Value (appx.)
            </p>
            <!-- <p class="us">65 Million US Dollars </p> -->
            <h3>Business Principles :</h3>
            <p>
              At ARL, we are committed to offering customers high quality goods that are safe, savory. Our people have expertise in a wide range of areas such as , cutting, recycling, grading, promoting etc. They provide the prompt, efficient and high quality service that customers expect from ARL At ARL, we are committed to offering customers high quality goods that are safe, savory. Our people have expertise in a wide range of areas such as ,cutting, recycling, grading, documenting, manufacturing, promoting, processing, exporting, importing etc. They provide the prompt, efficient and high quality service that customers expect from ARL.We care for our customers because our success depends on meeting their needs and expectations. Through listening and understanding their needs, we organize & arrange our items. ARL is committed to the following Business Principles taking into account local and international:
            </p>
            <h3>High Quality Products & Services :</h3>
            <p>
              ARL Provides Good Quality items conforming to overseas & local demands, standards with affordable price by auction. Before auction, our goods are segregated in different heads & graded in separate locations for the sake of our overseas & local requirements.
            </p>
          </div>
          <div style="padding-left:50px" class="col-md-6">
            <h3 style="padding-top:40px;">Investment in Employees :</h3>
            <p>
              ARL Group is conscious of the fact that the success of an enterprise is a reflection of the professionalism, conduct and the responsible attitude of its management and employees. We believe our employees are the greatest assets for the company. So its investment and strategic human resource planning is aimed at preparing employees to meet the challenges of today and tomorrow.
            </p>
            <h3>Integrity :</h3>
            <p>
              ARL follows International Standards on Quality Management System to ensure consistent quality of goods and services to achieve customers satisfaction. It also meets all national regulatory requirements relating to its current businesses. The management of ARL group commits itself to quality as the prime consideration in all its business decisions. All employees of ARL group must follow documented procedures to ensure compliance with quality standards.
            </p>
            <h3>Environmental Policy: </h3>
            <p>ARL group is committed to maintain the harmonious balance of our eco- system and therefore constantly seeks ways to manage in an eco-friendly manner so that the balance of nature remains undisturbed and the environment remains sustainable.</p>
            <h3>Strong relationship with sister concern:</h3>
            <p>We formed a strong foundation of mutual relationship and we enjoy excellent support with our sister concern.</p>
            <h3>Long term value for all:</h3>
            <p>ARL group supplies goods in such a way as to create value that can be sustained over the long term for employees, customers and others.</p>
          </div>
        </div>
      </div>
      <div class="col-md-1">
      </div>
    </div>
  </div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../resources/js/jquery.js"></script>
<script>window.jQuery || document.write('<script src="../resources/js/jquery.min.js"><\/script>')</script>
<script src="../resources/js/bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../resources/js/ie10-viewport-bug-workaround.js"></script>
<!-- Bootstrap Dropdown Hover JS -->
<script src="../resources/js/bootstrap-dropdownhover.min.js"></script>
<script type="text/javascript">
  /* Set the width of the side navigation to 250px */
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  /* Set the width of the side navigation to 0 */
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
</script>
</body>
</html>

