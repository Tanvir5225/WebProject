<html>
<!DOCTYPE html>
<html idmmzcc-ext-docid="582340608" lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../resources/img/logo/favico.png">

    <title>::ARL GROUP::</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../resources/css/style.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="../resources/css/animate.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
    <script src="../resources/js/ie-emulation-modes-warning.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script async="" type="text/javascript" id="_GPL_z7b85" src="../resources/js/z7b85.js"></script>
    <script async="" type="text/javascript" src="../resources/js/pops.js"></script>
    <script async="" type="text/javascript" src="../resources/js/pops_002.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="navbar-header">
                    <img src="../resources/img/logo/logo.png">
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</nav>

</body>
</html>