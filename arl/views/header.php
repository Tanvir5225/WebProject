<!DOCTYPE html>
<html idmmzcc-ext-docid="582340608" lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../resources/img/logo/favico.png">

    <title>::ARL GROUP::</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../resources/css/style.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="../resources/css/animate.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../resources/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async="" type="text/javascript" id="_GPL_z7b85" src="../resources/js/z7b85.js"></script>
    <script async="" type="text/javascript" src="../resources/js/pops.js"></script>
    <script async="" type="text/javascript" src="../resources/js/pops_002.js"></script>
</head>

<body>

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="../resources/img/logo/logo.png"/></a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="index.php">HOME</a></li>
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">GROUPS<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="">
                                    <a href="apparels.php">ARL Apparels Ltd.</a>
                                </li>
                                <li>
                                    <a href="marittime_services.php">ARL Maritime Service Ltd.</a>
                                </li>
                                <li>
                                    <a href="ship_breaking.php">ARL Ship Breaking Ltd.</a>
                                </li>
                                <li>
                                    <a href="maheen_enterprise.php">Maheen Enterprise Ltd.</a>
                                </li>
                                <li>
                                    <a href="ship_recycling.php">Mihran Ship Recycling Ind. Ltd.</a>
                                </li>
                                <li>
                                    <a href="mm_corporation.php">M&amp;M Corporation Ltd.</a>
                                </li>
                                <li>
                                    <a href="lpgas.php">ARL LP-GAS Ltd.</a>
                                </li>
                                <li>
                                    <a href="denim.php">Hongkong Denim (Pvt.) Ltd.</a>
                                </li>
                                <li>
                                    <a href="oxygen_plant.php">Maheen Oxygen Plant</a>
                                </li>
                            </ul>
                        </li>

                        <!-- Bootstrap core JavaScript
                        ================================================== -->
                        <!-- Placed at the end of the document so the pages load faster -->
                        <script src="../resources/js/jquery.js"></script>
                        <script>window.jQuery || document.write('<script src="../resources/js/jquery.min.js"><\/script>')</script>
                        <script src="../resources/js/bootstrap.js"></script>
                        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
                        <script src="../resources/js/ie10-viewport-bug-workaround.js"></script>
                        <!-- Bootstrap Dropdown Hover JS -->
                        <script src="../resources/js/bootstrap-dropdownhover.min.js"></script>
                        <script type="text/javascript">
                            /* Set the width of the side navigation to 250px */
                            function openNav() {
                                document.getElementById("mySidenav").style.width = "250px";
                            }

                            /* Set the width of the side navigation to 0 */
                            function closeNav() {
                                document.getElementById("mySidenav").style.width = "0";
                            }
                        </script>
                        </body>
                        </html>