<!DOCTYPE html>
<html idmmzcc-ext-docid="582340608" lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/logo/favico.png">

    <title>::ARL GROUP::</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async="" type="text/javascript" id="_GPL_z7b85" src="js/z7b85.js"></script>
    <script async="" type="text/javascript" src="js/pops.js"></script>
    <script async="" type="text/javascript" src="js/pops_002.js"></script>
</head>

<body>

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="img/logo/logo.png"/></a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <!-- <ul class="nav navbar-nav">
                         <li class="active"><a href="#">Home</a></li>
                         <li><a href="#about">About</a></li>
                         <li><a href="#contact">Contact</a></li>
                         <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                             <li><a href="#">Action</a></li>
                             <li><a href="#">Another action</a></li>
                             <li><a href="#">Something else here</a></li>
                             <li role="separator" class="divider"></li>
                             <li class="dropdown-header">Nav header</li>
                             <li><a href="#">Separated link</a></li>
                             <li><a href="#">One more separated link</a></li>
                           </ul>
                         </li>
                       </ul>-->
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="index.php">HOME</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">GROUPS <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="">
                                    <a href="apparels.php">ARL Apparels Ltd.</a>
                                </li>
                                <li>
                                    <a href="marittime_services.php">ARL Maritime Service Ltd.</a>
                                </li>
                                <li>
                                    <a href="ship_breaking.php">ARL Ship Breaking Ltd.</a>
                                </li>
                                <li>
                                    <a href="maheen_enterprise.php">Maheen Enterprise Ltd.</a>
                                </li>
                                <li>
                                    <a href="ship_recycling.php">Mihran Ship Recycling Ind. Ltd.</a>
                                </li>
                                <li>
                                    <a href="mm_corporation.php">M&amp;M Corporation Ltd.</a>
                                </li>
                                <li>
                                    <a href="lpgas.php">ARL LP-GAS Ltd.</a>
                                </li>
                                <li>
                                    <a href="denim.php">Hongkong Denim (Pvt.) Ltd.</a>
                                </li>
                                <li>
                                    <a href="oxygen_plant.php">Maheen Oxygen Plant</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">MANAGEMENT <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="">
                                    <a href="history.php">History and Organisation</a>
                                    <a href="msg_chairman.php">Message From Chairman</a>
                                    <a href="msg_md.php">Message From M.D.</a>
                                    <a href="msg_director.php">Message From Director</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="contact.php">CONTACT</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</nav>
</html>
<!-- Header ended-->