<html>
<div class="">
    <img width="100%;" class="img img-responsive" src="img/banner/banner2.png"/>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row">
                    <div style="padding-right:50px;" class="col-md-6">
                        <h2 style="color:#E1BF3A;">ARL GROUP</h2>
                        <p>
                            Atiur Rahman Laskar (ARL) Group, located in Port Chittagong, a renowned group of companies consisting distinct industrial concern - ISO-Certified Ship Breaking & Recycling Industry, Apparels Industry, Maritime Service, Export & Import, LP Gas, Oxygen Plant, Garments.<br/>
                            ARL was established in 2006 and is today become one of the prominent Group of Companies in Bangladesh. The company has capacity to handle and operate its concerns by expertise and skilled personnel.<br/>
                        </p>
                        <h3>VISSION</h3>
                        <p>
                            To meet the market needs and build a competitive position as efficiently and profitable by establishing a unique position in the competitive market of Bangladesh through goods and services of best quality in Ship Breaking & Recycling Industry, Apparel Industry, Maritime Service, Export & Import service, LP Gas Plant, Oxygen Plant and Garments Industry.<br/>
                            Asset Value (appx.)
                        </p>
                        <!-- <p class="us">65 Million US Dollars </p> -->
                        <h3>Business Principles :</h3>
                        <p>
                            At ARL, we are committed to offering customers high quality goods that are safe, savory. Our people have expertise in a wide range of areas such as , cutting, recycling, grading, promoting etc. They provide the prompt, efficient and high quality service that customers expect from ARL At ARL, we are committed to offering customers high quality goods that are safe, savory. Our people have expertise in a wide range of areas such as ,cutting, recycling, grading, documenting, manufacturing, promoting, processing, exporting, importing etc. They provide the prompt, efficient and high quality service that customers expect from ARL.We care for our customers because our success depends on meeting their needs and expectations. Through listening and understanding their needs, we organize & arrange our items. ARL is committed to the following Business Principles taking into account local and international:
                        </p>
                        <h3>High Quality Products & Services :</h3>
                        <p>
                            ARL Provides Good Quality items conforming to overseas & local demands, standards with affordable price by auction. Before auction, our goods are segregated in different heads & graded in separate locations for the sake of our overseas & local requirements.
                        </p>
                    </div>
                    <div style="padding-left:50px;" class="col-md-6">
                        <h3 style="padding-top:40px;">Investment in Employees :</h3>
                        <p>
                            ARL Group is conscious of the fact that the success of an enterprise is a reflection of the professionalism, conduct and the responsible attitude of its management and employees. We believe our employees are the greatest assets for the company. So its investment and strategic human resource planning is aimed at preparing employees to meet the challenges of today and tomorrow.
                        </p>
                        <h3>Integrity :</h3>
                        <p>
                            ARL follows International Standards on Quality Management System to ensure consistent quality of goods and services to achieve customers satisfaction. It also meets all national regulatory requirements relating to its current businesses. The management of ARL group commits itself to quality as the prime consideration in all its business decisions. All employees of ARL group must follow documented procedures to ensure compliance with quality standards.
                        </p>
                        <h3>Environmental Policy: </h3>
                        <p>ARL group is committed to maintain the harmonious balance of our eco- system and therefore constantly seeks ways to manage in an eco-friendly manner so that the balance of nature remains undisturbed and the environment remains sustainable.</p>
                        <h3>Strong relationship with sister concern:</h3>
                        <p>We formed a strong foundation of mutual relationship and we enjoy excellent support with our sister concern.</p>
                        <h3>Long term value for all:</h3>
                        <p>ARL group supplies goods in such a way as to create value that can be sustained over the long term for employees, customers and others.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
</html>