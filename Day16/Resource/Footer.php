<html>
<div class="footer">
    <div class="container consame">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="footmenu">
                            <li><a href="index.php">HOME</a></li>
                            <li><a href="#">GROUP</a></li>
                            <li><a href="#">MANAGEMENT</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                        <br/><br/>
                        <p style="color:#9EA3A8; font-size:13px; border-top:1px solid #394671; padding-left:20px;margin-top:150px;">
                            ARL GROUP &copy; All Rights Reserved <?php echo date('Y'); ?> | Powered By : <a style="color:#9EA3A8;text-decoration:none;" target="_blank" href="http://www.olineit.com">Oline IT</a>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img width="100%" src="img/logo/fright.jpg"/>
                        <div class="row">
                            <div class="col-md-6">
                                <p style="color:#9EA3A8; font-size:13px; padding-top:5px;">
                                    Company Name : ARL GROUP<br/>
                                    Type : Group of Concerns<br/>
                                    Year of Establishment : 11. 09. 1991<br/>
                                    Board of Directors : 04 (Four) persons<br/>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p style="color:#9EA3A8; font-size:13px; padding-top:5px;">
                                    CORP. OFFICE  :<br>
                                    <i class="fa fa-map-marker"></i> House No. 61/C, Road No. 2,Khulsi Hills,
                                    Chittagong, Bangladesh.<br>
                                    <i class="fa fa-phone"></i> +880 1711-762559,+880312551231-32
                                    Fax: +880312552655<br>
                                    <i class="fa fa-envelope"></i> info@arlbd.com<br>
                                    <i class="fa fa-link"></i> www.arlbd.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Bootstrap Dropdown Hover JS -->
<script src="js/bootstrap-dropdownhover.min.js"></script>
<script type="text/javascript">
    /* Set the width of the side navigation to 250px */
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
</body>
</html>